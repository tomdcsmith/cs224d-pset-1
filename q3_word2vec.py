import numpy as np
import random

from q1_softmax import softmax
from q2_gradcheck import gradcheck_naive
from q2_sigmoid import sigmoid, sigmoid_grad

def normalizeRows(x):
    """ Row normalization function """
    # Implement a function that normalizes each row of a matrix to have unit length
    
    ### YOUR CODE HERE

    # 1. Square each element
    # 2. Sum each row
    # 3. sqrt each sum
    # 4. Divide each element in row by that sqrt

    # return np.power(x, 2)  # 1
    # return np.sum(np.power(x, 2), axis=1)  # 2
    # return np.sqrt(np.sum(np.power(x, 2), axis=1)) # 3

    return x / np.sqrt(np.sum(np.power(x, 2), axis=1))[:,None]

    ### END YOUR CODE


def test_normalize_rows():
    print "Testing normalizeRows..."
    x = normalizeRows(np.array([[3.0,4.0],[1, 2]])) 
    # the result should be [[0.6, 0.8], [0.4472, 0.8944]]
    print x
    assert (x.all() == np.array([[0.6, 0.8], [0.4472, 0.8944]]).all())
    print ""


def softmaxCostAndGradient(predicted, target, outputVectors, dataset):
    """ Softmax cost function for word2vec models """
    
    # Implement the cost and gradients for one predicted word vector  
    # and one target word vector as a building block for word2vec     
    # models, assuming the softmax prediction function and cross      
    # entropy loss.                                                   
    
    # Inputs:                                                         
    # - predicted: numpy ndarray, predicted word vector (\hat{v} in 
    #   the written component or \hat{r} in an earlier version)
    # - target: integer, the index of the target word               
    # - outputVectors: "output" vectors (as rows) for all tokens     
    # - dataset: needed for negative sampling, unused here.         
    
    # Outputs:                                                        
    # - cost: cross entropy cost for the softmax word prediction    
    # - gradPred: the gradient with respect to the predicted word   
    #        vector                                                
    # - grad: the gradient with respect to all the other word        
    #        vectors                                               
    
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!                                                  
    
    ### YOUR CODE HERE

    z1 = np.dot(outputVectors, predicted)
    y_hat = softmax(z1)
    cost = -1 * np.log(y_hat[target])

    d1 = y_hat
    d1[target] -= 1

    gradPred = np.dot(outputVectors.T, d1)
    grad = np.outer(d1, predicted)

    ## TODO check if working

    ### END YOUR CODE
    
    return cost, gradPred, grad


def test_softmaxCostAndGradient():
    predicted = np.array([3, 4])
    target = 2
    outputVectors = np.array([[1, 1],
                      [1, 0],
                      [2, 0]])
    dataset = None

    test_yhat = softmax(np.array([7, 3, 6]))
    # should be np.array([ 0.72139918,  0.01321289,  0.26538793])

    test_cost = -1 * np.log(test_yhat[target])
    # should be about 1.326562637

    test_d1 = test_yhat
    test_d1[target] -= 1
    # should be about np.array([ 0.72139918,  0.01321289,  -0.73461207])

    test_gradPred = np.array([1.2653879287722418, 0.72139918427396865])
    # about [1.2653879287722418, 0.72139918427396865]
    test_grad = np.outer(test_d1, predicted)
    # about array([[ 2.16419754,  2.88559672],
    #    [ 0.03963867,  0.05285156],
    #    [-2.20383621, -2.93844828]])

    cost_output, gradPred_output, grad_output = softmaxCostAndGradient(predicted, target,
                                                                       outputVectors, dataset)

    assert (cost_output == test_cost)
    assert (gradPred_output.all() == test_gradPred.all())
    assert (grad_output.all() == test_grad.all())

    print "cost_output %s" % cost_output
    print "test_cost %s" % test_cost
    print "gradPred_output %s" % gradPred_output
    print "test_gradPred %s" % test_gradPred
    print "grad_output %s" % grad_output
    print "test_grad %s" % test_grad


def get_negative_samples(target, outputVectors, dataset, K):
    negative_samples = []
    indices = []
    for _ in range(K):
        idx = dataset.sampleTokenIdx()
        while idx == target:
            idx = dataset.sampleTokenIdx()
        indices.append(idx)
        negative_samples.append(outputVectors[idx])
    negative_samples = np.array(negative_samples)
    return negative_samples, indices


def negSamplingCostAndGradient_a(predicted, target, outputVectors, dataset,
    K=10):
    """ Negative sampling cost function for word2vec models """

    # Implement the cost and gradients for one predicted word vector  
    # and one target word vector as a building block for word2vec     
    # models, using the negative sampling technique. K is the sample  
    # size. You might want to use dataset.sampleTokenIdx() to sample  
    # a random word index. 
    # 
    # Note: See test_word2vec below for dataset's initialization.
    #                                       
    # Input/Output Specifications: same as softmaxCostAndGradient     
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!
    
    ### YOUR CODE HERE


    # 1. get matrix of negative examples
    # 2. get negative dot product of each word vector in these with predicted
    # 3. run each result through sigmoid function
    # 4. get ln of each of these results

    negative_samples, neg_indices = get_negative_samples(target, outputVectors, dataset, K)

    sig_1 = sigmoid(np.dot(outputVectors, predicted)[target])
    pos_val = -1 * np.log(sig_1)

    neg_pred_dot = np.dot(negative_samples, predicted)
    sig_2 = sigmoid(-1 * neg_pred_dot)
    neg_val = np.sum(np.log(sig_2))
    cost = pos_val - neg_val

    gradPred_p1 = (sig_1 - 1) * outputVectors[target]
    sigall_sub_one = sig_2 - 1
    sig_tar_outVec_dot = np.dot(negative_samples.T, sigall_sub_one)
    gradPred_p2 = np.sum(sig_tar_outVec_dot, axis=0)
    gradPred = gradPred_p1 + gradPred_p2

    grad = np.zeros(outputVectors.shape)
    grad[target] += (sig_1 - 1) * predicted
    for idx in neg_indices:
        grad[idx] += (1 - sig_2[idx]) * predicted


    ### END YOUR CODE
    
    return cost, gradPred, grad


def negSamplingCostAndGradient(predicted, target, outputVectors, dataset,
                               K=10):
    """ Negative sampling cost function for word2vec models """

    # Implement the cost and gradients for one predicted word vector
    # and one target word vector as a building block for word2vec
    # models, using the negative sampling technique. K is the sample
    # size. You might want to use dataset.sampleTokenIdx() to sample
    # a random word index.
    #
    # Note: See test_word2vec below for dataset's initialization.
    #
    # Input/Output Specifications: same as softmaxCostAndGradient
    # We will not provide starter code for this function, but feel
    # free to reference the code you previously wrote for this
    # assignment!

    ### YOUR CODE HERE


    # 1. get matrix of negative examples
    # 2. get negative dot product of each word vector in these with predicted
    # 3. run each result through sigmoid function
    # 4. get ln of each of these results

    gradPred = np.zeros(predicted.shape)
    grad = np.zeros(outputVectors.shape)

    labels = [[1] + [-1 for _ in range(K)]]
    indices = [target]
    for _ in range(K):
        idx = dataset.sampleTokenIdx()
        while idx == target:
            idx = dataset.sampleTokenIdx()
        indices.append(idx)

    vecs = outputVectors[indices, :]

    dots = np.dot(vecs, predicted)
    alt_dots = (dots * labels).flatten()
    sigs = sigmoid(alt_dots)

    cost = -1 * np.sum(np.log(sigs))

    ## problem below here?

    g = (sigs - 1) * labels
    update_value = np.dot(g, vecs)
    gradPred += update_value.flatten()

    g_transpose = g.transpose()
    predicted_reshape = predicted.reshape((1, predicted.shape[0]))
    grad_temp = np.dot(g_transpose, predicted_reshape)
    for i in xrange(K+1):
        grad[indices[i]] += grad_temp[i]

    ### END YOUR CODE

    return cost, gradPred, grad


def skipgram(currentWord, C, contextWords, tokens, inputVectors, outputVectors, 
    dataset, word2vecCostAndGradient = softmaxCostAndGradient):
    """ Skip-gram model in word2vec """

    # Implement the skip-gram model in this function.

    # Inputs:                                                         
    # - currrentWord: a string of the current center word           
    # - C: integer, context size                                    
    # - contextWords: list of no more than 2*C strings, the context words                                               
    # - tokens: a dictionary that maps words to their indices in    
    #      the word vector list                                
    # - inputVectors: "input" word vectors (as rows) for all tokens           
    # - outputVectors: "output" word vectors (as rows) for all tokens         
    # - word2vecCostAndGradient: the cost and gradient function for 
    #      a prediction vector given the target word vectors,  
    #      could be one of the two cost functions you          
    #      implemented above

    # Outputs:                                                        
    # - cost: the cost function value for the skip-gram model       
    # - grad: the gradient with respect to the word vectors         
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!

    ### YOUR CODE HERE

    cost = 0
    gradIn = np.zeros(inputVectors.shape)
    gradOut = np.zeros(outputVectors.shape)
    predicted = inputVectors[tokens[currentWord]]
    for context_word in contextWords:
        target = tokens[context_word]
        cur_cost, cur_gradPred, cur_grad = word2vecCostAndGradient(predicted=predicted, target=target,
                                                                   outputVectors=outputVectors, dataset=dataset)
        cost += cur_cost
        gradIn[tokens[currentWord]] += cur_gradPred
        gradOut += cur_grad

    ### END YOUR CODE
    
    return cost, gradIn, gradOut

def cbow(currentWord, C, contextWords, tokens, inputVectors, outputVectors, 
    dataset, word2vecCostAndGradient = softmaxCostAndGradient):
    """ CBOW model in word2vec """

    # Implement the continuous bag-of-words model in this function.            
    # Input/Output specifications: same as the skip-gram model        
    # We will not provide starter code for this function, but feel    
    # free to reference the code you previously wrote for this        
    # assignment!

    #################################################################
    # IMPLEMENTING CBOW IS EXTRA CREDIT, DERIVATIONS IN THE WRIITEN #
    # ASSIGNMENT ARE NOT!                                           #  
    #################################################################

    ### YOUR CODE HERE

    cost = 0
    gradIn = np.zeros(inputVectors.shape)
    gradOut = np.zeros(outputVectors.shape)

    predicted = outputVectors[tokens[currentWord]]
    for context_word in contextWords:
        target = tokens[context_word]
        cur_cost, cur_gradPred, cur_grad = word2vecCostAndGradient(predicted=predicted, target=target,
                                                                   outputVectors=inputVectors, dataset=dataset)
        cost += cur_cost
        gradOut[tokens[currentWord]] += cur_gradPred
        gradIn += cur_grad



    ### END YOUR CODE
    
    return cost, gradIn, gradOut

#############################################
# Testing functions below. DO NOT MODIFY!   #
#############################################

def word2vec_sgd_wrapper(word2vecModel, tokens, wordVectors, dataset, C, word2vecCostAndGradient = softmaxCostAndGradient):
    batchsize = 50
    cost = 0.0
    grad = np.zeros(wordVectors.shape)
    N = wordVectors.shape[0]
    inputVectors = wordVectors[:N/2,:]
    outputVectors = wordVectors[N/2:,:]
    for i in xrange(batchsize):
        C1 = random.randint(1,C)
        centerword, context = dataset.getRandomContext(C1)
        
        if word2vecModel == skipgram:
            denom = 1
        else:
            denom = 1
        
        c, gin, gout = word2vecModel(centerword, C1, context, tokens, inputVectors, outputVectors, dataset, word2vecCostAndGradient)
        cost += c / batchsize / denom
        grad[:N/2, :] += gin / batchsize / denom
        grad[N/2:, :] += gout / batchsize / denom
        
    return cost, grad

def test_word2vec():
    # Interface to the dataset for negative sampling
    dataset = type('dummy', (), {})()
    def dummySampleTokenIdx():
        return random.randint(0, 4)

    def getRandomContext(C):
        tokens = ["a", "b", "c", "d", "e"]
        return tokens[random.randint(0,4)], [tokens[random.randint(0,4)] \
           for i in xrange(2*C)]
    dataset.sampleTokenIdx = dummySampleTokenIdx
    dataset.getRandomContext = getRandomContext

    random.seed(31415)
    np.random.seed(9265)
    dummy_vectors = normalizeRows(np.random.randn(10,3))
    dummy_tokens = dict([("a",0), ("b",1), ("c",2),("d",3),("e",4)])
    print "==== Gradient check for skip-gram ===="
    gradcheck_naive(lambda vec: word2vec_sgd_wrapper(skipgram, dummy_tokens, vec, dataset, 5), dummy_vectors)
    gradcheck_naive(lambda vec: word2vec_sgd_wrapper(skipgram, dummy_tokens, vec, dataset, 5, negSamplingCostAndGradient), dummy_vectors)
    print "\n==== Gradient check for CBOW      ===="
    gradcheck_naive(lambda vec: word2vec_sgd_wrapper(cbow, dummy_tokens, vec, dataset, 5), dummy_vectors)
    gradcheck_naive(lambda vec: word2vec_sgd_wrapper(cbow, dummy_tokens, vec, dataset, 5, negSamplingCostAndGradient), dummy_vectors)

    print "\n=== Results ==="
    print skipgram("c", 3, ["a", "b", "e", "d", "b", "c"], dummy_tokens, dummy_vectors[:5,:], dummy_vectors[5:,:], dataset)
    print skipgram("c", 1, ["a", "b"], dummy_tokens, dummy_vectors[:5,:], dummy_vectors[5:,:], dataset, negSamplingCostAndGradient)
    print cbow("a", 2, ["a", "b", "c", "a"], dummy_tokens, dummy_vectors[:5,:], dummy_vectors[5:,:], dataset)
    print cbow("a", 2, ["a", "b", "a", "c"], dummy_tokens, dummy_vectors[:5,:], dummy_vectors[5:,:], dataset, negSamplingCostAndGradient)

if __name__ == "__main__":
    # test_normalize_rows()
    test_word2vec()
    # test_softmaxCostAndGradient()
